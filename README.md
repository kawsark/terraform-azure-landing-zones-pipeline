## terraform-azure-landing-zones-pipeline
This Project runs a GitLab CI/CD Pipeline to create and configure Workspaces in TFE/TFC. It is an example Developer inteface for IaC to allow for a streamlined developer experience while mantaining security and flexibility. 

**Key design elements:**
- A _Workspace-vending-machine_ that can be run 1000s of times a day based on a configuration (implemented as `config.json`)
- Credential injection abstracted from the developer. Ephemeral Azure credentials and TFC/TFE API tokens are obtained from a Vault server.
- Resources are separated into Privilged and Application level workspaces. 

### Pipeline details
When the pipeline is run, it executes the following tasks:
1. Creates a _Privileged_ and _Unprivileged_ Workspace based on the configuration in [config.json](scripts/python/config.json). For each Workspace, it performs the following:
    1. Associates a VCS Repository with the Workspace based on pre-configured OAuth ID
    1. Configures Workspace settings such as Terraform version, VCS Working directory, VCS Branch, Workspace variables etc.
    1. Injects Azure credentials based on Azure Secrets Engine Roles configuration on a Vault server. 
    1. Associates a pre-defined Sentinel policy set with the privileged Workspace.
    1. Creates a Run trigger for the Unprivileged Workspace so that a Run is auto-queued once the Privileged Workspace Run completes

1. Triggers a Run in the Privileged Workspace.

### Steps
1. Setup the JWT Authentication method, Azure Secrets Engine, and  for GitLab CI/CD Runners/
   1. Please see official [Azure Secrets Engine learn guide](https://learn.hashicorp.com/tutorials/vault/azure-secrets) and [blog post](https://medium.com/hashicorp-engineering/onboarding-the-azure-secrets-engine-for-vault-f09d48c68b69) for configuring the Azure Secrets Engine
   1. Please also see [JWT Auth Method](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/) and [Terraform Cloud Secrets Engine learn guide](https://learn.hashicorp.com/tutorials/vault/terraform-secrets-engine).
1. Configure a dedicated or shared Runner for GitLab CI/CD
1. Edit the [config.json](scripts/python/config.json) file to include the correct VCS associations
1. Run GitLab CI/CD pipeline via the UI or API. 
   1. Set a `PREFIX=demo` CI/CD Pipeline variable when running the pipeline multiple times.

### TODO
- Allow specifying Azure Secrets Engine paths in Vault via `config.json` or environment variables
- Add stage for re-running Apply for updating infrastructure
- Add pipeline stage for running Destroy
- Allow Sentinel policy set association for both privileged and non-privileged Workspace
