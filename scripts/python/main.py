from terrasnek.api import TFC
from terrasnek.exceptions import TFCHTTPNotFound
from terrasnek.exceptions import TFCHTTPUnprocessableEntity
import hvac
import os
import json
import time
import sys

# TODO: get the tfc token from Vault
TFC_TOKEN = os.getenv("TFC_TOKEN", None)
TFC_URL = os.getenv("TFC_URL", None)  # ex: https://app.terraform.io

VAULT_ADDR = os.getenv("VAULT_ADDR", "http://localhost:8200")
VAULT_TOKEN = os.getenv("VAULT_TOKEN", None)

VAULT_AZURE_PRIV_ROLE="subscription-role"
VAULT_AZURE_UNPRIV_ROLE="developer-role"
VAULT_AZURE_PATH="azure-demo"

# TODO: Allow dynamic population of Policy Set and Developer Team ID
POLICY_SET_ID = os.getenv("POLICY_SET_ID", "polset-bgk7gXWfucfaFMtS")  
DEVELOPER_TEAM_ID = os.getenv("DEVELOPER_TEAM_ID", "team-PoQruc2kgrUNsQvK")  

# Read variables passed from upstream Pipeline
PREFIX = os.getenv("PREFIX", None) 
VCS_REPO = os.getenv("VCS_NAME", None)
VCS_NAMESPACE = os.getenv("VCS_NAMESPACE", None)
VCS_BRANCH = os.getenv("VCS_BRANCH", None)

# Staging and Production GitLab Namespaces for Manual Apply
privileged_project_groups = ['hc-demos-staging','hc-demos-production']

# Override Sensitive setting for Azure credentials
SENSITIVE_CREDS = os.getenv("SENSITIVE_CREDS", None)

def get_azure_creds(vault_client, vault_azure_role):
	azure_config = vault_client.secrets.azure.read_config(mount_point=VAULT_AZURE_PATH)
	
	sensitive_creds = True
	if SENSITIVE_CREDS and SENSITIVE_CREDS == "False":
		sensitive_creds = False

	attempts = 0
	max = 3
	azure_creds = None
	while attempts < max:
		print(f"INFO: Getting Azure Creds from {vault_azure_role} ({attempts}/{max})")
		azure_creds = vault_client.secrets.azure.generate_credentials(mount_point=VAULT_AZURE_PATH, name=vault_azure_role)
		if azure_creds["client_id"] and len(azure_creds["client_id"]) > 0:
			break
		attempts+=1
	
	subscription_id = azure_config["subscription_id"]
	tenant_id = azure_config["tenant_id"]
	client_id = azure_creds["client_id"]
	client_secret = azure_creds["client_secret"]

	merged_creds_data = [
		{
			"key": "ARM_SUBSCRIPTION_ID",
			"value": subscription_id,
			"sensitive": False,
			"hcl": False,
			"category": "env"
		},
		{
			"key": "ARM_TENANT_ID",
			"value": tenant_id,
			"sensitive": sensitive_creds,
			"hcl": False,
			"category": "env"
		},
		{
			"key": "ARM_CLIENT_ID",
			"value": client_id,
			"sensitive": sensitive_creds,
			"hcl": False,
			"category": "env"
		},
		{
			"key": "ARM_CLIENT_SECRET",
			"value": client_secret,
			"sensitive": sensitive_creds,
			"hcl": False,
			"category": "env"
		},
	]

	return merged_creds_data


def build_create_ws_payload(ws):
	return {
		"data": {
			"type": "workspaces",
			"attributes": {
				"name": ws["ws_name"],
				"terraform_version": ws["tf_version"],
				"working-directory": ws["working_dir"],
				"auto-apply": ws["auto_apply"],
				"global-remote-state": True,
				"vcs-repo": {
					"identifier": ws["vcs_repo"],
					"oauth-token-id": ws["vcs_oauth_id"],
					"branch": ws["branch"]
				}
			}
		}
	}


def build_create_var_payload(ws_id, var):
	# TODO: have defaults for each variable
	return  {
		"data": {
			"type": "vars",
			"attributes": {
				"key": var["key"],
				"value": var["value"],
				"description": "Set via TFE API",
				"category": var["category"],
				"hcl": var["hcl"],
				"sensitive": var["sensitive"]
			},
			"relationships": {
				"workspace": {
					"data": {
						"id": ws_id,
						"type": "workspaces"
					}
				}
			}
		}
	}

def build_team_access_payload(ws_id,team_id):
	return {
		"data": {
			"attributes": {
			"access": "custom",
			"runs": "apply",
			"variables": "write",
			"state-versions": "read",
			"plan-outputs": "read",
			"sentinel-mocks": "read",
			"workspace-locking": "true"
			},
			"relationships": {
			"workspace": {
				"data": {
				"type": "workspaces",
				"id": ws_id
				}
			},
			"team": {
				"data": {
				"type": "teams",
				"id": team_id
				}
			}
			},
			"type": "team-workspaces"
		}
}

# https://www.terraform.io/docs/cloud/api/team-access.html#add-team-access-to-a-workspace
def build_team_access_payload_staging(ws_id,team_id):
	return {
		"data": {
			"attributes": {
				"access": "read"
			},
			"relationships": {
				"workspace": {
					"data": {
					"type": "workspaces",
					"id": ws_id
					}
				},
				"team": {
					"data": {
					"type": "teams",
					"id": team_id
					}
				}
			},
			"type": "team-workspaces"
		}
}


def build_create_run_payload(ws_id):
	return {
		"data": {
			"attributes": {
				"message": "Run created via the TFE API"
			},
			"type":"runs",
			"relationships": {
				"workspace": {
					"data": {
						"type": "workspaces",
						"id": ws_id
					}
				},
			}
		}
	}


def create_ws(ws_config, tfc_client, vault_client):
	org_name = ws_config["org_name"]
	ws_name = ws_config["ws_name"]

	print(f"INFO: Looking up Org {org_name} and Workspace {ws_name}")

	tfc_client.set_org(org_name)

	workspace = None
	
	try:
		workspace = tfc_client.workspaces.show(workspace_name=ws_name)
	except TFCHTTPNotFound:
		pass
	
	if workspace is None:
		print(f"INFO: Workspace {ws_name} not found, creating new.")
		create_ws_payload = build_create_ws_payload(ws_config)
		workspace = tfc_client.workspaces.create(create_ws_payload)
		time.sleep(2)
	else:
		print(f"INFO: Found existing Workspace {ws_name}")

	return workspace

def remove_vars(ws_id):
	# This function removes all previously set variables
	ws_vars = tfc_client.workspace_vars.list(ws_id)
	if ws_vars and "data" in ws_vars:
		for ws_var in ws_vars["data"]:
			if ws_var and "id" in ws_var:
				tfc_client.workspace_vars.destroy(ws_id,ws_var["id"])

def populate_tf_vars(ws_id, ws_config):
	# Once the workspace is created, inject the user defined variables
	print("INFO: Populating TF variables...")
	for var in ws_config["variables"]:
		create_var_payload = build_create_var_payload(ws_id, var)
		try:
			tfc_client.workspace_vars.create(ws_id, create_var_payload)
		except TFCHTTPUnprocessableEntity as error:
			print(f"WARN: Could to set variable: {error}")

	print("INFO: TF variables populated.")


def populate_env_vars(ws_id, vault_azure_role):
	# Once the workspace is created, get the variables that are needed from Vault
	print("INFO: Getting Azure Creds from Vault, this may take a moment...")
	azure_creds_vars = get_azure_creds(vault_client, vault_azure_role)
	print("INFO: Azure Creds retrieved from Vault.")

	# Inject the credentials into the workspace
	print("INFO: Populating environment variables...")
	for var in azure_creds_vars:
		create_var_payload = build_create_var_payload(ws_id, var)
		try:	
			tfc_client.workspace_vars.create(ws_id, create_var_payload)
		except TFCHTTPUnprocessableEntity as error:
			print(f"WARN: Could to set variable: {error}")
	
	print("INFO: Environment variables populated.")


def trigger_run(ws_id):
	# Trigger a run on the workspace
	create_run_payload = build_create_run_payload(ws_id)
	created_run = tfc_client.runs.create(create_run_payload)
	print(created_run)

	# Wait for the run to complete, then create the next workspace


if __name__ == "__main__":
	if not PREFIX:
		print("ERROR: Please see a PREFIX Variable to use this Pipeline")
		sys.exit()

	config = {}
	with open("config.json", "r") as infile:
		config = json.loads(infile.read())

	privileged_project_group = VCS_NAMESPACE in privileged_project_groups
	
	# Mutate the Privileged Workspace name and VCS Settings
	ws_name = config["privileged"]["ws_name"]
	if VCS_REPO:
		ws_name = ''.join((PREFIX,"-",VCS_REPO))

		# Override VCS Settings
		if VCS_NAMESPACE:
			config["privileged"]["vcs_repo"] = '/'.join([VCS_NAMESPACE,VCS_REPO])
			#config["privileged"]["branch"] = VCS_BRANCH
			vcs_repo=config["privileged"]["vcs_repo"]
			print(f"INFO: Overriding VCS Settings: {vcs_repo}")

		# If this is not a staging or production Namespace, then set to auto apply
		if not privileged_project_group:
			config["privileged"]["auto_apply"] = "true"
			print(f"INFO: Setting TFE Workspace Apply Method to Auto Apply")

	else:
		ws_name = ''.join((PREFIX,"-",ws_name))
	
	config["privileged"]["ws_name"] = ws_name

	# Mutate the Non privileged Workspace name
	ws_name = config["unprivileged"]["ws_name"]
	ws_name = ''.join((PREFIX,"-",ws_name))
	config["unprivileged"]["ws_name"] = ws_name

	# Update the Resource Group name
	config["privileged"]["variables"][0]["value"] = PREFIX
	# rg_name = config["privileged"]["variables"][0]["value"] 
	# if VCS_REPO:
	# 	rg_name = ''.join((PREFIX,"-",VCS_REPO))
	# else:
	# 	rg_name = ''.join((PREFIX,"-",rg_name))
	# config["privileged"]["variables"][0]["value"] = rg_name

	tfc_client = TFC(TFC_TOKEN, url=TFC_URL, verify=False)

	TIMEOUT_TIME=120
	vault_client = hvac.Client(url=VAULT_ADDR, token=VAULT_TOKEN, timeout=TIMEOUT_TIME, namespace=os.getenv('VAULT_NAMESPACE', ""))

	priv_config = config["privileged"]
	unpriv_config = config["unprivileged"]

	# Create the AKS cluster w/ elevated credentials
	priv_ws = create_ws(priv_config, tfc_client, vault_client)
	priv_ws_id = priv_ws["data"]["id"]

	# Attach the policy set to the privileged workspace
	attach_policy_set_payload = {
		"data": [
			{
				"id": priv_ws_id,
				"type": "workspaces"
			},
  		]
	}
	tfc_client.policy_sets.attach_policy_set_to_workspaces(POLICY_SET_ID, attach_policy_set_payload)

	# Configure team access for privileged workspace
	team_access_payload = None
	if privileged_project_group:
		team_access_payload = build_team_access_payload_staging(priv_ws_id, DEVELOPER_TEAM_ID)
	else:
		team_access_payload = build_team_access_payload(priv_ws_id, DEVELOPER_TEAM_ID)
	try:
		tfc_client.team_access.add_team_access(team_access_payload)
	except TFCHTTPUnprocessableEntity as error:
		print(f"WARN: Could not add Team access for Privileged Workspace, a previous one may be present: {error}")

	# Populate variables for privileged workspace
	remove_vars(priv_ws_id)
	populate_tf_vars(priv_ws_id, priv_config)
	populate_env_vars(priv_ws_id, VAULT_AZURE_PRIV_ROLE)

	# Create a second workspace, with the dev VM repo, and have the dependency on the
	# first workspace (https://gitlab.com/kawsark/terraform-azure-devvm-aks)
	unpriv_ws = create_ws(unpriv_config, tfc_client, vault_client)
	unpriv_ws_id = unpriv_ws["data"]["id"]

	# Configure team access for unprivileged workspace
	team_access_payload = build_team_access_payload(unpriv_ws_id, DEVELOPER_TEAM_ID)
	try:
		tfc_client.team_access.add_team_access(team_access_payload)
	except TFCHTTPUnprocessableEntity as error:
		print(f"WARN: Could not add Team access for Non-Privileged Workspace, a previous one may be present: {error}")

	# Attach a run trigger to the workspace for when the privileged workspace finishes
	run_trigger_payload = {
		"data": {
			"relationships": {
				"sourceable": {
					"data": {
						"id": priv_ws_id,
						"type": "workspaces"
					}
				}
			}
		}
	}
	# Add Run trigger
	try:
		tfc_client.run_triggers.create(unpriv_ws_id, run_trigger_payload)
	except TFCHTTPUnprocessableEntity as error:
		print(f"WARN: Could not add Run trigger, a previous one may be present: {error}")

	# KK: Adding delay for Azure cred to propagate
	time.sleep(10)

	# Now that the run triger is in place, trigger the run on the privileged workspace
	trigger_run(priv_ws_id)

	# Add the private workspace name to the variables
	priv_ws_name = priv_ws["data"]["attributes"]["name"]
	priv_ws_name_var = {
		"key": "tfc_aks_workspace",
		"value": priv_ws_name,
		"sensitive": False,
		"hcl": False,
		"category": "terraform"
	}
	unpriv_config["variables"].append(priv_ws_name_var)
	remove_vars(unpriv_ws_id)
	populate_tf_vars(unpriv_ws_id, unpriv_config)

	# Populate with Azure credentials again (developer credentials)
	# TODO: make these use a different, less privileged role.
	populate_env_vars(unpriv_ws_id, VAULT_AZURE_UNPRIV_ROLE)
